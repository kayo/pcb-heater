{sqrt, floor} = Math

r_ = # Ohm × m² / m
  Cu: 17.5e-9

a_ = # 1 / °C
  Cu: 4.3e-3

@w0 = (r, a, d, t, p, u, l, w, s)->
  r = r_[r] if r of r_
  a = a_[a] if a of a_
  (sqrt((a*t+1)*4*d*l*p*r*w+d*d*s*s*u*u)-d*s*u)/(2*d*u)

@kicad_mod = (name, W, L, s, w, t = 0.15, l = 15, b = 21)->
  W *= 1e3
  L *= 1e3
  s *= 1e3
  w *= 1e3

  # t - text/drawing thickness
  # l - layer number (15 - Copper_Front)
  # b - bound layer (21 - SilkS_Front)

  o = [
    "PCBNEW-LibModule-V1  #{do (new Date).toString}"
    "# encoding utf-8"
    "Units mm"
    "$INDEX"
    name
    "$EndINDEX"
    "$MODULE #{name}"
    "Po 0 0 0 15 548E7D80 00000000 ~~"
    "Li #{name}"
    "Sc 0"
    "AR "
    "Op 0 0 0"
  ]

  drawText = (n, x, y, d)->
    o.push "T#{n} #{x} #{y} 1 1 0 #{t} N V #{b} N \"#{d}\""
  
  drawBound = (x0, y0, x1, y1)->
    o.push "DS #{x0} #{y0} #{x1} #{y0} #{t} #{b}"
    o.push "DS #{x1} #{y0} #{x1} #{y1} #{t} #{b}"
    o.push "DS #{x1} #{y1} #{x0} #{y1} #{t} #{b}"
    o.push "DS #{x0} #{y1} #{x0} #{y0} #{t} #{b}"
  
  drawLine = (x0, y0, x1, y1)->
    o.push "DS #{x1} #{y1} #{x0} #{y0} #{w} #{l}"
  
  drawArc = (x0, y0, x1, y1, a)->
    o.push "DA #{x0} #{y0} #{x1} #{y1} #{(a * 10)|0} #{w} #{l}"

  addPad = (n, x, y, s = "R")->
    o.push "$PAD"
    o.push "Sh \"#{n}\" #{s} #{w} #{w} 0 0 0"
    o.push "Dr 0 0 0"
    o.push "At SMD N 00888000"
    o.push "Ne 0 \"\""
    o.push "Po #{x} #{y}"
    o.push "$EndPAD"

  dx = w + s
  ar = dx / 2 # arc radius
  ll = L - (w * 2 + s) # line length
  ln = floor W / (w + s) # lines number
  W1 = ln * w + (ln - 1) * s
  
  x0 = -W1 / 2 + w / 2
  y0 = -L / 2 + w + s / 2
  
  x1 = x0 + W1 - w
  y1 = y0 + ll

  drawText 0, 0, L / 2 + 1, name
  drawText 1, 0, -L / 2 - 1, "VAL**"
  
  drawBound -W / 2, -L / 2, W / 2, L / 2
  
  drawLine x0, y0, x0, y1
  
  for i in [1 ... ln]
    x = x0 + i * dx - ar
    if i % 2
      drawArc x, y1, x + ar, y1, 180
    else
      drawArc x, y0, x + ar, y0, -180
    x += ar
    drawLine x, y0, x, y1
  
  addPad 1, x0, y0
  addPad 2, x1, if ln % 2 then y1 else y0
  
  [
    o..., [
      "$EndMODULE #{name}"
      "$EndLIBRARY"
      ""
    ]...
  ].join "\n"

@svg_shape = (name, W, L, s, w, t = 0.15, l = "red", b = "blue")->
  W *= 1e3
  L *= 1e3
  s *= 1e3
  w *= 1e3
  
  ###
  l - wire layer color
  b - bound rect color
  t - text/drawing thickness
  ###

  styl = (style={})->
    ("#{key.replace /[A-Z]/g, (c)-> "-#{do c.toLowerCase}"}:#{val}" for key, val of style when key and val?).join ";"
  
  attr = (attrs={})->
    (" #{key.replace /[A-Z]/g, (c)-> "-#{do c.toLowerCase}"}=\"#{if typeof val is "object" then styl val else val}\"" for key, val of attrs when key and val?).join ""
  
  svg = (attrs, nodes...)->
    "<svg#{attr attrs}>#{nodes}</svg>"
  
  path = (attrs, segments...)->
    "<path#{attr attrs} d=\"#{segments.join " "}\"/>"
  
  rect = (attrs)->
    "<rect#{attr attrs}/>"

  ar = w / 2 + s / 2 # arc radius
  ll = L - (w * 2 + s) # line length
  ln = floor W / (w + s) # lines number
  W1 = ln * w + (ln - 1) * s
  
  x0 = w / 2 + (W - W1) / 2
  y0 = w + s / 2
  
  x1 = x0 + W1 - w
  y1 = y0 + ll
  
  dx = w + s
  
  "<?xml version=\"1.0\" standalone=\"no\"?>"+
  svg
    id: name
    width: W
    height: L
    version: "1.1"
    xmlns: "http://www.w3.org/2000/svg"
    rect
      id: "#{name}-edge"
      style:
        stroke: b
        strokeWidth: t
        fill: "none"
      x: t/2
      y: t/2
      width: W-t
      height: L-t
    path
      id: "#{name}-wire"
      width: W1
      height: L
      stroke: l
      strokeWidth: w
      fill: "none"
      (
        seg = [
          "M#{x0},#{y0}"
          "V#{y1}"
        ]
        
        for i in [1 ... ln]
          seg.push "A#{ar},#{ar} 0 0,#{if i % 2 then 0 else 1} #{x0 + i * dx},#{if i % 2 then y1 else y0}"
          seg.push "V#{if i % 2 then y0 else y1}"
        
        seg
      )
