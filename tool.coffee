#!/usr/bin/env coffee

Args = require "arg-parser"
{writeFileSync} = require "fs"
{stdout} = process

{w0, kicad_mod, svg_shape} = require "./heater"

args = new Args "tool", "0.0.1", "PCB-heater generator", "Generate PCB heater with parameters"

args.add
  name: "f"
  desc: "Output format"
  switches: [
    "-f"
    "--format"
  ]
  value: "svg,kicad"

args.add
  name: "o"
  desc: "Output module file"
  switches: [
    "-o"
    "--output"
  ]
  value: "heater.mod"

args.add
  name: "n"
  desc: "Module name"
  switches: [
    "-n"
    "--name"
  ]
  value: "heater"

args.add
  name: "r"
  desc: "Specific resistance on initial temperature [Ohm × m² / m]"
  switches: [
    "-r"
    "--sr"
  ]
  value: "17.5e-9 (Cu)"

args.add
  name: "a"
  desc: "Temperature coefficient of resistance [1 / °C]"
  switches: [
    "-a"
    "--tcr"
  ]
  value: "4.3e-3 (Cu)"

args.add
  name: "t"
  desc: "Required temperature delta (i.e. t_target - t_initial) [K]"
  switches: [
    "-t"
    "--dt"
  ]
  value: "80"

args.add
  name: "d"
  desc: "Wire layer height [m]"
  switches: [
    "-d"
    "--height"
  ]
  value: "35e-6 (35 um)"

args.add
  name: "w"
  desc: "Required width of heater plate"
  switches: [
    "-w"
    "--width"
  ]
  value: "80e-3 (8 cm)"

args.add
  name: "l"
  desc: "Required length of heater plate"
  switches: [
    "-l"
    "--length"
  ]
  value: "60e-3 (6 cm)"

args.add
  name: "s"
  desc: "Required spacing between tracks"
  switches: [
    "-s"
    "--spacing"
  ]
  value: "0.6e-3 (0.6 mm)"

args.add
  name: "p"
  desc: "Heater power [W]"
  switches: [
    "-p"
    "--power"
  ]
  value: "10"

args.add
  name: "u"
  desc: "Heater voltage [V]"
  switches: [
    "-u"
    "--voltage"
  ]
  value: "5"

#
# Parsing arguments
#

return unless do args.parse

{params:{f, o, n, r, a, t, d, w, l, s, p, u}} = args

n ?= "heater"
#o ?= "#{n}.mod"
f ?= if o then (o.match /\.([^\.]+)$/)?[1] else "svg"

r ?= "Cu"
a ?= "Cu"
t ?= 80
d ?= 35e-6
w ?= 80e-3
l ?= 60e-3
s ?= 0.6e-3
p ?= 10
u ?= 5

w0_ = w0 r, a, d, t, p, u, l, w, s

switch f
  when "svg"
    x = svg_shape n, w, l, s, w0_
  when "mod"
    x = kicad_mod n, w, l, s, w0_

if o
  writeFileSync o, x
else
  stdout.write x
