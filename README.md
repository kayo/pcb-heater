PCB Heater generator
====================

Theoretical reasons
-------------------

A theory is based on a very simple physical equations:

```maxima

Q[1]: r0 = (r * l0) / (w0 * d); /* r - specific resistance, l0 - wire length, w0 - wire width, d - layer height, r0 - resistance on initial temperature (20°C) */
Q[2]: r1 = r0 * (1 + a * t); /* a - temperature coefficient of resistance, t - temperature delta, r1 - resistance on target temperature (t0 + t) */
Q[3]: p = u ^ 2 / r1; /* u - powering voltage, p - heater power */
Q[4]: w * l = (w0 + s) * l0; /* s - spacing between tracks, w - heater board width, l - heater board length */

Q[5]: Q[2], Q[1];
Q[6]: solve(Q[3], r1);
Q[7]: solve(Q[4], l0);
Q[8]: Q[5], Q[7];
Q[9]: Q[8], Q[6];
Q[10]: solve(Q[9], w0);

```

The result of Q[10] is wire width:

```maxima

w0=(sqrt((4*a*d*l*p*r*t+4*d*l*p*r)*w+d^2*s^2*u^2)−d*s*u)/(2*d*u)

```

Console tool usage
------------------

```plain

PCB-heater generator
usage: tool.coffee [options]

 -a, --tcr=4.3E-3 (CU)           Temperature coefficient of resistance [1 / °C]
 -d, --height=35E-6 (35 UM)      Wire layer height [m]
 -f, --format=SVG,KICAD          Output format
 -h, --help                      display help & usage
 -l, --length=60E-3 (6 CM)       Required length of heater plate
 -n, --name=HEATER               Module name
 -o, --output=HEATER.MOD         Output module file
 -p, --power=10                  Heater power [W]
 -r, --sr=17.5E-9 (CU)           Specific resistance on initial temperature [Ohm × m² / m]
 -s, --spacing=0.6E-3 (0.6 MM)   Required spacing between tracks
 -t, --dt=80                     Required temperature delta (i.e. t_target - t_initial) [K]
 -u, --voltage=5                 Heater voltage [V]
 -v, --version                   display cli name & version
 -w, --width=80E-3 (8 CM)        Required width of heater plate

 Generate PCB heater with parameters

```

We uses a base SI units.
